#performs a single instance of training on a given
# training set (arraylist),
# validation set (reslist),
# path for saving the resulting weights(weightpath,
# callback to load for the training instance(callback),
# the number of epochs to train for(epochs)
def train(array_list, res_list, weightpath, callback, epochs):
    test_set = np.concatenate((array_list[0], array_list[1], array_list[2], array_list[3]), axis=0)  # train in
    train_set = np.concatenate((res_list[0], res_list[1], res_list[2], res_list[3]), axis=0)  # train out
    model.load_weights(tf.train.latest_checkpoint(weightpath))
    model.fit(test_set, train_set, epochs=epochs, callbacks=[callback])  # Todo

#trains a modell while using all training data
def single_training():
    charge = 1
    while charge <= 5:

        arraylist = []
        reslist = []
        split = 0
        while split < 5:
            arraylist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(charge) + "_seq.npy",
                     "rb")))
            reslist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(
                    charge) + "b_int.npy",
                     "rb")))
            split += 1


        train(array_list=arraylist, res_list=reslist, weightpath=checkpoint_dirs_b[charge - 1],
                  callback=callbacks_b[charge - 1], epochs=6)
        charge += 1

    charge = 1
    while charge <= 5:

        arraylist = []
        reslist = []
        split = 0

        while split < 5:
            arraylist.append(np.load(
                open(
                    "D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(charge) + "_seq.npy",
                    "rb")))
            reslist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(
                    charge) + "y_int.npy",
                     "rb")))
            split += 1

        train(array_list=arraylist, res_list=reslist, weightpath=checkpoint_dirs_y[charge - 1],
                  callback=callbacks_y[charge - 1], epochs=6)
        charge += 1


#performs prediction on the holdout set and saves the resulting predictions
# it should be noted that for the sake of being able to continue bad coding habits I renamed the holdout set
# into what a 6th training splits name would have been named
def holdout():
    charge = 1
    while charge <= 5:
        val_set = np.load(
            open(
                "D:\Temp\PBL\Dataframe_numpy\charge_splits\\5_" + str(charge) + "_seq.npy",
                "rb"))
        model.load_weights(tf.train.latest_checkpoint(checkpoint_dirs_b[charge - 1]))
        np.save(f"D:\Temp\\results\prediction_{charge}b", np.asarray(model.predict(
            val_set), dtype=object).astype(np.float32), allow_pickle=True)
        np.save(f"D:\Temp\\results\input_set_{charge}b",
                val_set, allow_pickle = True)
        charge += 1

    charge = 1
    while charge <= 5:
        val_set = np.load(
            open(
                "D:\Temp\PBL\Dataframe_numpy\charge_splits\\5_" + str(charge) + "_seq.npy",
                "rb"))
        model.load_weights(tf.train.latest_checkpoint(checkpoint_dirs_y[charge - 1]))
        np.save(f"D:\Temp\\results\prediction_{charge}y", np.asarray(model.predict(
            val_set), dtype=object).astype(np.float32), allow_pickle=True)
        np.save(f"D:\Temp\\results\input_set_{charge}y",
                val_set, allow_pickle=True)
        charge += 1