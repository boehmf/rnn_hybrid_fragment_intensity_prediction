import numpy as np
#path of the original splits
splitpath = ""

#path of the folder for data subsets
charge_split_path = ""


def split_list(a_list):
    half = len(a_list) // 2
    return [a_list[:half], a_list[half:]]

#divides all splits by precursor charge and saves the resulting files to a single folder
def split_charge():
    split = 5

    while split <= 5:

        seq = np.load(
            open(splitpath + "\split" + str(split) + "\s" + str(split) + "_test_seq.npy",
                 "rb"),allow_pickle=True).tolist()

        numb = np.load(
            open(splitpath + "\split" + str(split) + "\s" + str(split) + "_test_int.npy",
                 "rb"),allow_pickle=True).tolist()
        pre = np.load(
            open(splitpath + "\split" + str(split) + "\s" + str(split) + "_test_pre.npy",
                 "rb"),allow_pickle=True).tolist()

        ints = [[], [], [], [], [], [], []]
        seqs = [[], [], [], [], [], [], []]

        i = 0
        while i < len(pre):
            seqs[int(pre[i]) - 1].append(seq[i])
            ints[int(pre[i]) - 1].append(numb[i])
            i += 1

        i = 0
        while i < 7:
            np.save(charge_split_path + "\charge_splits\\" + str(split) + "_" + str(i + 1) + "_seq",
                    np.asarray(seqs[i], dtype=object).astype(np.float32),
                    allow_pickle=True)
            np.save(charge_split_path + "\charge_splits\\" + str(split) + "_" + str(i + 1) + "_int",
                    np.asarray(ints[i], dtype=object).astype(np.float32),
                    allow_pickle=True)
            i += 1
        split += 1

#devides the output of split_charge() into b and y type sequnences and safes the results to the same folder
def split_ion_type():
    split = 5

    while split <= 5:
        i = 0
        while i < 7:
            ints = np.load(
                open(charge_split_path + "\charge_splits\\" + str(split) + "_" + str(i + 1) + "_int.npy",
                     "rb")).tolist()

            b = []
            y = []

            l = 0
            while l < len(ints):
                line = split_list(ints[l])
                b.append(line[0])
                y.append(line[1])
                l += 1

            np.save(charge_split_path + "\charge_splits\\" + str(split) + "_" + str(i + 1) + "b_int",
                        np.asarray(b, dtype=object).astype(np.float32),
                        allow_pickle=True)
            np.save(charge_split_path + "\charge_splits\\" + str(split) + "_" + str(i + 1) + "y_int",
                        np.asarray(y, dtype=object).astype(np.float32),
                        allow_pickle=True)
            i += 1
        split += 1

split_charge()
split_ion_type()

