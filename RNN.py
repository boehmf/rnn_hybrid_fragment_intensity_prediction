import tensorflow as tf
import numpy as np
import os

#returns the model architecture
def build_model():
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(input_dim=30, output_dim=7),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(30)),
        tf.keras.layers.Dense(28),
    ])
    return model

model = build_model()
#this function compiles the model, determines certain hyperparameters as well as the metrics that need to be tracked during training
model.compile(optimizer='adam', loss=tf.keras.losses.mean_squared_error,
              metrics=[tf.keras.losses.cosine_similarity, tf.keras.losses.mean_squared_error,
                       tf.keras.losses.mean_absolute_error])


#the following loads the models weightsets and their directories in lists to make them easily accessible during training/validation
callbacks_b = []
callbacks_y = []
checkpoint_dirs_b = []
checkpoint_dirs_y = []
for i in range(1, 6):
    checkpoint_dir = f'./training_checkpoints{i}b'
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt_{epoch}")
    callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_prefix, save_weights_only=True, save_freq="epoch")
    checkpoint_dirs_b.append(checkpoint_dir)
    callbacks_b.append(callback)

    checkpoint_dir = f'./training_checkpoints{i}y'
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt_{epoch}")
    callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_prefix, save_weights_only=True, save_freq="epoch")
    checkpoint_dirs_y.append(checkpoint_dir)
    callbacks_y.append(callback)
checkpoint_dir = './training_checkpoints'
checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt_{epoch}")



