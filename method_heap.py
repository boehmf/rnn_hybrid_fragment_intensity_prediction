
#the following includes functions that are either outdated, broken or maybe broken for various reasons.
# They are still included here for the sake of completeness


#trains the model on all of the weightset and evaluates the model of the holdout set for a specific charge-ion typ combination
def holdout(array_list, res_list, position, charge):
    metrics = []

    test_set = np.concatenate((array_list[0], array_list[1], array_list[2], array_list[3], array_list[4]),
                              axis=0)  # train in
    train_set = np.concatenate((res_list[0], res_list[1], res_list[2], res_list[3], res_list[4]), axis=0)  # train out

    epochs = 5
    if charge == 1 or charge == 7 or charge == 6:
        epochs = 15
    if charge == 5:
        epochs = 8

    model.fit(test_set, train_set, epochs=epochs, callbacks=[checkpoint_callback])

    val_set = np.load(
        open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\5" + "_" + str(charge) + "_seq.npy",
             "rb"))
    res_set = np.load(
        open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\5" + "_" + position + "_int.npy",
             "rb"))

    metrics.append(model.evaluate(x=val_set, y=res_set))

    # add spectral to metrics
    np.save("D:\Temp\\result", np.asarray(model.predict(
        val_set), dtype=object).astype(np.float32), allow_pickle=True)
    metrics[-1].append(getspectral(res_set))
    log('final: \n', position)
    log(metrics, position)
    avgmetrics = [0, 0, 0, 0]
    c = 0
    r = 0
    while c < 5:
        while r < 4:
            avgmetrics[r] = metrics[c][r]
            r += 1
        c += 1

    for i in avgmetrics:
        i / 5
    log("\navg\n", position)
    log(avgmetrics, position)

#runs the holdout() method for all charge-ion typ combinations
def run_holdout():
    charge = 1
    while charge <= 5:

        arraylist = []
        reslist = []
        split = 0

        while split < 5:
            arraylist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(charge) + "_seq.npy",
                     "rb")))
            reslist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(
                    charge) + "b_int.npy",
                     "rb")))
            split += 1

        holdout(array_list=arraylist, res_list=reslist, position=(str(charge) + 'b'), charge=charge)
        charge += 1
        if charge == 2:
            charge += 1

    charge = 1
    while charge <= 5:

        arraylist = []
        reslist = []
        split = 0

        while split < 5:
            arraylist.append(np.load(
                open(
                    "D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(charge) + "_seq.npy",
                    "rb")))
            reslist.append(np.load(
                open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\" + str(split) + "_" + str(
                    charge) + "y_int.npy",
                     "rb")))
            split += 1

        holdout(array_list=arraylist, res_list=reslist, position=(str(charge) + 'y'), charge=charge)
        charge += 1
        if charge == 2:
            charge += 1

#does a specific k in k-fold validation
def k_fold(array_list, res_list, position):
    # TODO delete checkpoints
    i = 0
    metrics = []
    while i < 5:
        val_set = array_list.pop(i)  # val in
        res_set = res_list.pop(i)  # val out
        test_set = np.concatenate((array_list[0], array_list[1], array_list[2], array_list[3]), axis=0)  # train in
        train_set = np.concatenate((res_list[0], res_list[1], res_list[2], res_list[3]), axis=0)  # train out
        model.fit(test_set, train_set, epochs=5, callbacks=[checkpoint_callback])
        metrics.append(model.evaluate(x=val_set, y=res_set))

        # add spectral to metrics
        np.save("D:\Temp\\result", np.asarray(model.predict(
            val_set), dtype=object).astype(np.float32), allow_pickle=True)
        metrics[-1].append(getspectral(res_set))

        array_list.insert(i, val_set)
        res_list.insert(i, res_set)
        i += 1

    log('final: \n', position)
    log(metrics, position)
    avgmetrics = [0, 0, 0, 0]
    c = 0
    r = 0
    while c < 5:
        while r < 4:
            avgmetrics[r] = metrics[c][r]
            r += 1
        c += 1

    for i in avgmetrics:
        i / 5
    log("\navg\n", position)
    log(avgmetrics, position)

#coordinates the k_fold() method to perform 5-fold validation
def run_kfold():
    charge = 1
    while charge <= 7:

        arraylist = []
        reslist = []
        split = 0

        while split < 5:
            arraylist.append(np.load(
                open("C:\Temp\PBL\Dataframe_numpy\charge_splits\seq\\" + str(split) + "_" + str(charge) + "_seq.npy",
                     "rb")))
            reslist.append(np.load(
                open("C:\Temp\PBL\Dataframe_numpy\charge_splits\\b_int\\" + str(split) + "_" + str(
                    charge) + "b_int.npy",
                     "rb")))
            split += 1

        k_fold(array_list=arraylist, res_list=reslist, position=(str(charge) + 'b'))
        charge += 1

    charge = 1
    while charge <= 7:

        arraylist = []
        reslist = []
        split = 0

        while split < 5:
            arraylist.append(np.load(
                open(
                    "C:\Temp\PBL\Dataframe_numpy\charge_splits\seq\\" + str(split) + "_" + str(charge) + "_seq.npy",
                    "rb")))
            reslist.append(np.load(
                open("C:\Temp\PBL\Dataframe_numpy\charge_splits\\y_int\\" + str(split) + "_" + str(
                    charge) + "y_int.npy",
                     "rb")))
            split += 1

        k_fold(array_list=arraylist, res_list=reslist, position=(str(charge) + 'y'))
        charge += 1

#a prototype model evaluation method
def model_testing():
    predict_set = np.load(open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\seq_two_data.npy", "rb"), allow_pickle=True)
    model.load_weights(tf.train.latest_checkpoint(checkpoint_dir))
    print(model.metrics_names)
    print(model.summary())
    print(model.evaluate(x=predict_set, y=testset))

#a prototype prediction method
def prediction():
    list = model.predict(
        np.load(open("D:\Temp\PBL\Dataframe_numpy\charge_splits\\seq_two_data.npy", "rb"), allow_pickle=True))
    np.save("D:\Temp\\result", np.asarray(list, dtype=object).astype(np.float32), allow_pickle=True)
